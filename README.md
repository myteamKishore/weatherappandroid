# Weather App

Weather Singapore is an intuitive and easy application, you have the current climate of Singapore and weather forecasts Singapore within 5 days.
Singapore Weather forecast is a very light android application which does not consume much of your smartphone resources.

# Requirements
  - Android 5.0 (API 21) or above
  - Build with Android Studio 3.4.1

# Features
  - Today Weather Forecast
  - Tomorrow Weather Forecast
  - Five days Weather Forecast
  
# Android Architecture Diagram
https://bitbucket.org/myteamKishore/weatherappandroid/src/master/Architecture%20Diagram.png

## Setup the project in Android studio and run tests.

  1. Download the project code, preferably using `git clone`.
  1. In Android Studio, select *File* | *Open...* and point to the `./build.gradle` file.
  1. Make sure you select "Unit Tests" as the test artifact in the "Build Variants" panel in Android Studio.
  1. Check out the relevant code:
      * The application code is located in `src/main/java`
      * Unit Tests are in `src/test/java`
  1. Create a test configuration with the JUnit4 runner: `org.junit.runners.JUnit4`
      * Open *Run* menu | *Edit Configurations*
      * Add a new *JUnit* configuration
      * Choose module *app*
      * Select the class to run by using the *...* button
  1. Run the newly created configuration

  The unit test will be ran automatically.

## Use Gradle on the command line.

  After downloading the projects code using `git clone` you'll be able to run the
  unit tests using the command line:

      ./gradlew test

  If all the unit tests have been successful you will get a `BUILD SUCCESSFUL`
  message.
## See the report.

  A report in HTML format is generated in `app/build/reports/tests`