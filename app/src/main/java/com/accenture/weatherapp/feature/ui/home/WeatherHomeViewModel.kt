package com.accenture.weatherapp.feature.ui.home

import android.text.format.DateUtils
import com.accenture.weatherapp.core.base.ViewStateViewModel
import com.accenture.weatherapp.core.enums.APIStatus
import com.accenture.weatherapp.core.extensions.calendar
import com.accenture.weatherapp.core.extensions.isTomorrow
import com.accenture.weatherapp.core.extensions.toDate
import com.accenture.weatherapp.core.helpers.Callback
import com.accenture.weatherapp.core.models.WeatherInfo
import com.accenture.weatherapp.core.models.WeatherReportResponse
import com.accenture.weatherapp.feature.data.home.usecases.WeatherForecastFiveDaysUseCase
import java.util.*

class WeatherHomeViewModel : ViewStateViewModel<WeatherHomeViewModel.ViewState>() {
    override fun getInitialState(): ViewState = ViewState()

    val weatherForecastFiveDaysUseCase = WeatherForecastFiveDaysUseCase()

    fun loadWeatherForecastApi() {
        setState(getState().copy(weatherReportApiStatus = APIStatus.LOADING))

        weatherForecastFiveDaysUseCase?.getWeatherForecastFiveDaysApi(
            "Singapore,sg",
            callback = object : Callback<WeatherReportResponse> {
                override fun onSuccess(response: WeatherReportResponse?) {

                    response
                    filterData(response)
                }

                override fun onError(error: Throwable) {
                    setState(getState().copy(weatherReportApiStatus = APIStatus.ERROR))
                }

            })
    }

    /*
    * Maps a [AccountDetailResponse] to [DepositAccount].
    */
    private fun filterData(response: WeatherReportResponse?) {
        val today = response?.list?.filter { DateUtils.isToday(it?.dtTxt?.toDate()?.time ?: 0) }
            ?.sortedBy { it?.dtTxt }

        val tomorrow =
            response?.list?.filter { it?.dtTxt?.toDate()?.isTomorrow() ?: false }?.sortedBy { it?.dtTxt }

        val fiveDays = response?.list?.groupBy { item ->
            calendar.time = item?.dtTxt?.toDate()
            calendar.get(Calendar.DAY_OF_MONTH)
        }?.map {
            it.value.sortedBy { it?.dtTxt }
        }?.map {
            it.getOrNull(0)
        }

        setState(
            getState().copy(
                weatherReportApiStatus = APIStatus.SUCCESS,
                today = today,
                tomorrow = tomorrow,
                fiveDays = fiveDays
            )
        )


    }

    /**
     * The view state for this view model.
     */
    data class ViewState(
        val today: List<WeatherInfo?>? = null,
        val tomorrow: List<WeatherInfo?>? = null,
        val fiveDays: List<WeatherInfo?>? = null,
        val weatherReportApiStatus: APIStatus = APIStatus.UNKNOWN
    )

}