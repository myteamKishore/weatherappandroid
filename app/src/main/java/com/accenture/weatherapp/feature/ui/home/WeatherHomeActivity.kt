package com.accenture.weatherapp.feature.ui.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.accenture.weatherapp.R
import com.accenture.weatherapp.core.base.BaseActivity
import com.accenture.weatherapp.core.enums.APIStatus
import com.accenture.weatherapp.feature.ui.home.widgets.WeatherHomePagerAdapter
import com.accenture.weatherapp.libs.ViewDialog
import kotlinx.android.synthetic.main.activity_main.*

class WeatherHomeActivity : BaseActivity() {

    private lateinit var weatherHomeViewModel: WeatherHomeViewModel
    private lateinit var viewDialog: ViewDialog
    private lateinit var weatherHomePagerAdapter: WeatherHomePagerAdapter

    private var isLoaded = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.initUI()
    }

    private fun initUI() {
        weatherHomeViewModel = ViewModelProviders.of(this).get(WeatherHomeViewModel::class.java)

        viewDialog = ViewDialog(this)
        this.observeViewModel()
    }

    private fun observeViewModel() {
        weatherHomeViewModel.viewState.observe(this, Observer { this.render(it) })
    }

    override fun onStart() {
        super.onStart()

        this.setupTabAndViewPager()
        if (!isLoaded) {
           // isLoaded = true
            weatherHomeViewModel.loadWeatherForecastApi()
        }
    }

    /**
     * initialize  Page Adapter
     */
    private fun setupTabAndViewPager() {
        weatherHomePagerAdapter = WeatherHomePagerAdapter(this, supportFragmentManager)
        tabs.setupWithViewPager(view_pager)
    }

    /**
     * Render the UI according to [nextState].
     */
    private fun render(nextState: WeatherHomeViewModel.ViewState) {

        when (nextState.weatherReportApiStatus) {
            APIStatus.LOADING -> {
                viewDialog.showDialog()
            }
            APIStatus.ERROR -> {
                viewDialog.hideDialog()
            }
            APIStatus.SUCCESS -> {
                viewDialog.hideDialog()

            }
        }

        weatherHomePagerAdapter.removeAll()

        nextState.today?.let {
            weatherHomePagerAdapter.addFrag(WeatherCategoryFragment.newInstance(it), "Today")
        }

        nextState.tomorrow?.let {
            weatherHomePagerAdapter.addFrag(WeatherCategoryFragment.newInstance(it), "Tomorrow")
        }

        nextState.fiveDays?.let {
            weatherHomePagerAdapter.addFrag(WeatherCategoryFragment.newInstance(it), "5 Days")
        }

        view_pager.adapter = weatherHomePagerAdapter

    }
}