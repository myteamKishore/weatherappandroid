package com.accenture.weatherapp.feature.ui.home.widgets

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.accenture.weatherapp.R

class WeatherListAdapter : RecyclerView.Adapter<WeatherListViewHolder>() {

  var items: List<Item?>? = listOf()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherListViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(
      R.layout.cell_weather_item,
      parent,
      false
    )
    return WeatherListViewHolder(view)
  }

  override fun onBindViewHolder(holder: WeatherListViewHolder, position: Int) {
    holder.bind(items?.getOrNull(position))
  }

  override fun getItemCount(): Int {
    return items?.size?:0
  }

  fun setWeatherItems(items: List<Item?>?) {
    this.items = items
    this.notifyDataSetChanged()
  }

  /**
   * Replace items with dummy loading data.
   */
  fun setLoading(count: Int) {
    this.items = List(count) { Item() }
    this.notifyDataSetChanged()
  }

  data class Item(
    val tempInCelsius: String? = "",
    val minTempInCelsius: String? = "",
    val maxTempInCelsius: String? = "",
    val humidity: String? = "",
    val windSpeed: String? = "",
    val time: String? = "",
    val date: String? = "",
    val pressure: String? = "",
    val weatherDesc: String? = "",
    val iconType: String? = "",
    val cloudPercentage: String? = ""
  )

}