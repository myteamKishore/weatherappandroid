package com.accenture.weatherapp.feature.data.home.usecases

import com.accenture.weatherapp.core.helpers.Callback
import com.accenture.weatherapp.core.models.WeatherReportResponse
import com.accenture.weatherapp.core.network.api.WeatherForecast

class WeatherForecastFiveDaysUseCase {

    fun getWeatherForecastFiveDaysApi(query: String, callback: Callback<WeatherReportResponse>) {
        WeatherForecast().apply {
            this.callback = callback
        }.execute(query)
    }
}