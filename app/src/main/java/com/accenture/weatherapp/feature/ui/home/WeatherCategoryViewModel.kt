package com.accenture.weatherapp.feature.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.accenture.weatherapp.core.base.ViewStateViewModel
import com.accenture.weatherapp.core.extensions.toCelsius
import com.accenture.weatherapp.core.extensions.toDate
import com.accenture.weatherapp.core.extensions.toFormat
import com.accenture.weatherapp.core.models.WeatherInfo
import com.accenture.weatherapp.feature.ui.home.widgets.WeatherListAdapter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class WeatherCategoryViewModel : ViewStateViewModel<WeatherCategoryViewModel.ViewState>() {
    override fun getInitialState(): ViewState = ViewState()






    fun setData(items: List<WeatherInfo?>?){

        setState(
            getState().copy(
               data = items?.map { it?.toWeatherListAdapterItem() }
            )
        )

    }

    /**
     * Map a [WeatherInfo] to [WeatherListAdapter.Item].
     */
    private fun WeatherInfo.toWeatherListAdapterItem() =
        WeatherListAdapter.Item(
            tempInCelsius = this.main?.temp?.toCelsius(),
            minTempInCelsius = this.main?.tempMin?.toCelsius(),
            maxTempInCelsius = this.main?.tempMax?.toCelsius(),
            humidity = "Humidity: ${this.main?.humidity} %",
            windSpeed = "Wind: ${this.wind?.speed} m/sec",
            time = this.dtTxt?.toDate()?.toFormat(),
            pressure = "Pressure: ${this.main?.pressure} hPa",
            weatherDesc = "${this.weather?.getOrNull(0)?.description?.toUpperCase()}",
            iconType = this.weather?.getOrNull(0)?.icon,
            cloudPercentage = "${this.clouds?.all} %"
        )


    /**
     * The view state for this view model.
     */
    data class ViewState(
        var data: List<WeatherListAdapter.Item?>? = null
    )
}