package com.accenture.weatherapp.feature.ui.home.widgets

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.accenture.weatherapp.core.extensions.findDrawable
import com.accenture.weatherapp.core.extensions.toDate
import com.accenture.weatherapp.core.extensions.toFormat
import kotlinx.android.synthetic.main.cell_weather_item.view.*


class WeatherListViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private var imWeatherIcon = itemView.imWeatherIcon as ImageView
    private var tvCelsius = itemView.tvCelsius as TextView
    private var tvTime = itemView.tvTime as TextView

    private var tvTempMax = itemView.tvTempMax as TextView
    private var tvTempMin = itemView.tvTempMin as TextView

    private var tvWeatherDesc = itemView.tvWeatherDesc as TextView
    private var tvHumidity = itemView.tvHumidity as TextView
    private var tvWindSpeed = itemView.tvWindSpeed as TextView
    private var tvPressure = itemView.tvPressure as TextView





    /**
     * Binds this view with [data].
     */
    fun bind(data: WeatherListAdapter.Item?) {
        tvCelsius.text = data?.tempInCelsius
        tvTempMax.text = data?.maxTempInCelsius
        tvTempMin.text = data?.minTempInCelsius
        imWeatherIcon.setImageResource(itemView.context.findDrawable("ic_${data?.iconType}"))
        tvWeatherDesc.text = data?.weatherDesc
        tvHumidity.text = data?.humidity
        tvTime.text=data?.time

        tvWindSpeed.text = data?.windSpeed
        tvPressure.text = data?.pressure

    }
}