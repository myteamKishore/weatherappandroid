package com.accenture.weatherapp.feature.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.accenture.weatherapp.R
import com.accenture.weatherapp.core.base.BaseFragment
import com.accenture.weatherapp.core.extensions.toCelsius
import com.accenture.weatherapp.core.models.WeatherInfo
import com.accenture.weatherapp.core.models.WeatherReportResponse
import com.accenture.weatherapp.feature.ui.home.widgets.WeatherListAdapter
import kotlinx.android.synthetic.main.fragment_main.*

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import java.io.Serializable
import java.util.Arrays.asList
import org.json.JSONArray




/**
 * A placeholder fragment containing a simple view.
 */
class WeatherCategoryFragment : BaseFragment() {

    private lateinit var weatherCategoryViewModel: WeatherCategoryViewModel
    private lateinit var weatherListAdapter: WeatherListAdapter


    init {
        weatherListAdapter = WeatherListAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        weatherCategoryViewModel = ViewModelProviders.of(this).get(WeatherCategoryViewModel::class.java)
        this.observeViewModel()

        (arguments?.getSerializable(ARG_Data) as? List<WeatherInfo?>?)?.let {
            weatherCategoryViewModel.setData(it)
            arguments?.remove(ARG_Data)
        }

    }

    private fun observeViewModel() {
        weatherCategoryViewModel.viewState.observe(this, Observer { this.render(it) })
    }

    override fun onStart() {
        super.onStart()
        this.setupWeatherAdapter()

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main, container, false)


    /**
     * Render the UI according to [nextState].
     */
    private fun render(nextState: WeatherCategoryViewModel.ViewState) {

        weatherListAdapter.setWeatherItems(nextState.data)
    }




    private fun setupWeatherAdapter() {
        this.rvWeather.addItemDecoration(
            DividerItemDecoration(
                rvWeather.context,
                DividerItemDecoration.VERTICAL
            )
        )

        this.rvWeather?.apply {

            layoutManager = LinearLayoutManager(activity)
            adapter = weatherListAdapter
        }
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_Data = "Argument_data"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(data: List<WeatherInfo?>?): WeatherCategoryFragment {

            return WeatherCategoryFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_Data, data as Serializable)
                }
            }
        }
    }
}