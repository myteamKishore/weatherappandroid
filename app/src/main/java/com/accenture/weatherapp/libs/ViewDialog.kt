package com.accenture.weatherapp.libs

import android.app.Activity
import android.app.Dialog
import android.view.Window
import com.accenture.weatherapp.R


class ViewDialog(var activity: Activity): Dialog(activity){
  var dialog: Dialog? = null
  var isVisible: Boolean = false

  fun showDialog() {
    dialog = Dialog(activity)
    dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
    //...set cancelable false so that it's never get hidden
    dialog?.setCancelable(false)
    //...that's the layout i told you will inflate later
    dialog?.setContentView(R.layout.view_loading)

    dialog?.show()

    isVisible = true
  }

  fun hideDialog() {
    dialog?.dismiss()

    isVisible = false
  }

}