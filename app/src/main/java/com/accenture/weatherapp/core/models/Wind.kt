package com.accenture.weatherapp.core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Wind(
    @SerialName("deg") val deg: Double? = null,
    @SerialName("speed") val speed: Double? = null
)