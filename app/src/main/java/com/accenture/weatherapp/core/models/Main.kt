package com.accenture.weatherapp.core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Main(
    @SerialName("grnd_level") val grndLevel: String? = null,
    @SerialName("humidity") val humidity: String? = null,
    @SerialName("pressure") val pressure: String? = null,
    @SerialName("sea_level") val seaLevel: String? = null,
    @SerialName("temp") val temp: String? = null,
    @SerialName("temp_kf") val tempKf: String? = null,
    @SerialName("temp_max") val tempMax: String? = null,
    @SerialName("temp_min") val tempMin: String? = null
)