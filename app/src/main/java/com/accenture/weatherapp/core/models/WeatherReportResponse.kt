package com.accenture.weatherapp.core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherReportResponse(
    @SerialName("city") val city: City? = null,
    @SerialName("cnt") val cnt: Int? = null,
    @SerialName("cod") val cod: String? = null,
    @SerialName("list") val list: List<WeatherInfo?>? = null,
    @SerialName("message") val message: Double? = null
)