package com.accenture.weatherapp.core.extensions

fun String.toCelsius(): String = "${"%.2f".format((this.toDouble() - 273.15))} °C"


