package com.accenture.weatherapp.core.extensions

import java.text.SimpleDateFormat
import java.util.*

val calendar: Calendar by lazy {
    Calendar.getInstance()
}

fun String.toDate(): Date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this)

fun Date.isTomorrow(): Boolean {
    val before = Calendar.getInstance()
    before.add(Calendar.DAY_OF_YEAR, 1)

    return this.after(calendar.time) && this.before(before.time)
}

fun Date.toFormat(format: String = "h:mma EEE dd MMM, yyyy"): String = SimpleDateFormat(format).format(this)






