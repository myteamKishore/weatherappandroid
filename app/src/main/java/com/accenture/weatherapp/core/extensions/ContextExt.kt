package com.accenture.weatherapp.core.extensions

import android.content.Context

fun Context.findDrawable(resId: String): Int = resources.getIdentifier(resId, "drawable", packageName)


