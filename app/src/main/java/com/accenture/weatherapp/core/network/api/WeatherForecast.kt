package com.accenture.weatherapp.core.network.api

import android.os.AsyncTask
import com.accenture.weatherapp.BuildConfig
import com.accenture.weatherapp.core.helpers.Callback
import com.accenture.weatherapp.core.models.WeatherReportResponse
import kotlinx.serialization.json.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.net.URL

class WeatherForecast : AsyncTask<String, Void, String>() {

    var callback: Callback<WeatherReportResponse>? = null

    override fun doInBackground(vararg param: String?): String {
        var response = "UNDEFINED"
        try {

            val url = URL("${BuildConfig.BASE_API_URL}forecast?q=${param.getOrNull(0)}&APPID=${BuildConfig.APPID}")
            val urlConnection = url.openConnection()

            val lines = urlConnection.getInputStream().use { it.bufferedReader().readText() }

            val topLevel = JSONObject(lines)

            response = topLevel.toString()

        } catch (e: Exception) {
            e.printStackTrace()
            callback?.onError(e)
        }

        return response
    }

    override fun onPostExecute(result: String?) {
         super.onPostExecute(result)

        val data = JSON.parse(WeatherReportResponse.serializer(), result ?: "")
        if (data.cod == "200")
            callback?.onSuccess(data)
        else
            callback?.onError(Throwable(message = "Invalid"))

    }


}