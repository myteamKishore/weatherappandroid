package com.accenture.weatherapp.core.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import java.io.IOException
import java.net.ConnectException

abstract class BaseActivity : AppCompatActivity(), LifecycleOwner {


}