package com.accenture.weatherapp.core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Coord(
    @SerialName("lat") val lat: String? = null,
    @SerialName("lon") val lon: String? = null
)