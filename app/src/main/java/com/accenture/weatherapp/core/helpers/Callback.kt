package com.accenture.weatherapp.core.helpers

interface Callback<T> {
    fun onSuccess(response: T?)
    fun onError(error: Throwable)
}