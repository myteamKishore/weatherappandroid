package com.accenture.weatherapp.core.enums

enum class APIStatus {
  LOADING,ERROR,SUCCESS,UNKNOWN
}