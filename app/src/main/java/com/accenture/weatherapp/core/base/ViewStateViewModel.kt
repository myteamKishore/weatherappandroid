package com.accenture.weatherapp.core.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * [ViewStateViewModel] is a base class for view models that use the view state pattern.
 *
 * The view state pattern represents UI state as a single, immutable object. Views
 * can then react to these view state changes using pure, idempotent functions.
 *
 * You are required to supply it a data class as type [T] to be used as the view
 * state. It is recommended to be a Kotlin data class for its immutability functions.
 */
abstract class ViewStateViewModel<T> : ViewModel() {

  /** The backing property for [viewState]. */
  private val _viewState: MutableLiveData<T> = MutableLiveData()

  /** The view model's observable view state. */
  val viewState: LiveData<T>
    get() = _viewState

  init {
    this.initializeViewModel()
  }

  /**
   * Initialize the view model.
   */
  private fun initializeViewModel() {
    _viewState.value = getInitialState()
  }

  /**
   * Returns the initial view state.
   */
  protected abstract fun getInitialState(): T

  /**
   * Returns the current view state.
   */
  protected fun getState(): T {
    return _viewState.value!!
  }

  /**
   * Sets the view state to [nextState].
   */
  protected fun setState(nextState: T) {
    _viewState.value = nextState
  }

}
