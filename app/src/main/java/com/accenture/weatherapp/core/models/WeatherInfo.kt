package com.accenture.weatherapp.core.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherInfo(
    @SerialName("clouds") val clouds: Clouds? = null,
    @SerialName("dt") val dt: Long? = null,
    @SerialName("dt_txt") val dtTxt: String? = null,
    @SerialName("main") val main: Main? = null,
    @SerialName("rain") val rain: Rain? = null,
    @SerialName("sys") val sys: Sys? = null,
    @SerialName("weather") val weather: List<Weather?>? = null,
    @SerialName("wind") val wind: Wind? = null
)