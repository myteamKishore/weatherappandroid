package com.accenture.weatherapp

import com.accenture.weatherapp.core.extensions.toCelsius
import com.accenture.weatherapp.core.extensions.toDate
import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UnitTest {
    @Test
    fun `Check the Date extension`() {

       val date= "2018-02-20 15:00:00"
        assertEquals(date.toDate(), SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date))
    }

    @Test
    fun `Check toCelsius`() {

        val temp= "268.169"
        assertEquals(temp.toCelsius(),"${"%.2f".format((temp.toDouble() - 273.15))} °C")
    }

}
